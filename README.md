CaRMS and NRMP match simulator using roth-peranson rust crate

dependencies
------------
```
rust
cargo with wasm32-unknown-unknown target
wasm-bindgen
npm
```

to build
--------
```
$ cd www
$ npm i
$ npm run build
```

then serve the files in `dist` with any webserver
