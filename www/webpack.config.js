const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
    entry: "./index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "",
        filename: "bundle.js",
    },
    mode: "production",
    plugins: [
        new CopyWebpackPlugin(['index.html'])
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: "babel-loader"
            },
            {
                test: /\.rs$/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            compact: true
                        }
                    },
                    {
                        loader: "rust-native-wasm-loader",
                        options: {
                            release: true,
                            wasmBindgen: {
                                wasm2es6js: true
                            }
                        }
                    }
                ]
            }
        ]
    }
};
