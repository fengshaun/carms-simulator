const rootReducer = (state = {applicantNum: 0, programNum: 0, applicants: {}, programs: {}}, action) => {
    switch (action.type) {
    case "ADD_APPLICANT":
        let aNum = state.applicantNum + 1;
        
        return {
            ...state,
            applicantNum: aNum,
            applicants: {
                ...state.applicants,
                [state.applicantNum]: {
                    id: state.applicantNum,
                    name: "",
                    rol: [],
                    matched: undefined,
                    pending: true
                }
            }
        };

    case "ADD_APPLICANT_ROL":
        return {
            ...state,
            applicants: {
                ...state.applicants,
                [action.id]: {
                    ...state.applicants[action.id],
                    rol: state.applicants[action.id].rol.concat([{id: action.rolId, rank: state.applicants[action.id].rol.length}])
                }
            }
        };

    case "ADD_PROGRAM":
        let pNum = state.programNum + 1;

        return {
            ...state,
            programNum: pNum,
            programs: {
                ...state.programs,
                [state.programNum]: {
                    id: state.programNum,
                    name: "",
                    rol: [],
                    capacity: action.capacity,
                    matches: [],
                }
            }
        };

    case "ADD_PROGRAM_ROL":
        return {
            ...state,
            programs: {
                ...state.programs,
                [action.id]: {
                    ...state.programs[action.id],
                    rol: state.programs[action.id].rol.concat([{id: action.rolId, rank: state.programs[action.id].rol.length}])
                }
            }
        };

    case "UPDATE_PROGRAM_CAPACITY":
        return {
            ...state,
            programs: {
                ...state.programs,
                [action.id]: {
                    ...state.programs[action.id],
                    capacity: action.capacity
                }
            }
        };
        
    default:
        console.log("REDUX ACTION " + JSON.stringify(action) + " NOT FOUND");
        return state;
    }
}

/* ACTIONS */

export const addApplicant = () => {
    return {
        type: "ADD_APPLICANT",
    }
}

export const addApplicantROL = (id, pid) => {
    return {
        type: "ADD_APPLICANT_ROL",
        id: id,
        rolId: pid
    }
}

export const addProgram = (capacity) => {
    return {
        type: "ADD_PROGRAM",
        capacity: capacity
    }
}

export const addProgramROL = (id, aid) => {
    return {
        type: "ADD_PROGRAM_ROL",
        id: id,
        rolId: aid
    }
}

export const updateProgramCapacity = (id, cap) => {
    return {
        type: "UPDATE_PROGRAM_CAPACITY",
        id: id,
        capacity: cap
    }
}

export default rootReducer;
