import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { addApplicant, addApplicantROL, addProgram, addProgramROL, updateProgramCapacity } from './store';

import './index.css';

class Participant extends React.Component {
    constructor(props, title, capacity) {
        super(props);
        this.title = title;

        this.state = {
            currentROLRankId: "",
            editingName: false,
            capacity: this.props.info.capacity,
        };
    }

    addROLRank = (e) => {
        if (e.key != "Enter") {
            return;
        }

        if (this.state.currentROLRankId.length <= 0) {
            return;
        }

        let rolRankNum = parseInt(this.state.currentROLRankId);
        if (isNaN(rolRankNum)) {
            alert("Programs and applicants must be referred to by their numeric ID.");
            return;
        }

        this.props.addROL(this.props.info.id, rolRankNum);
        this.setState((s) => {
            return {
                currentROLRankId: ""
            };
        });
    }

    updateROLRankId = (e) => {
        let v = e.target.value;

        this.setState((s) => {
            return {
                currentROLRankId: v
            };
        });
    }

    updateCapacity = (e) => {
        let n = parseInt(e.target.value);
        this.props.updateCapacity(this.props.info.id, n);
    }

    render() {
        let rol = this.props.info.rol.map((v, i) => <li className="list-group-item" key={i}>{v.id}</li>);

        let header;
        if (this.props.info.capacity > 0) {
            header =
                <div className="column card-header">
                <div>
                {this.title} {this.props.info.id}
            </div>
                <div>
                capacity:  <input
            className="capacity"
            type="number"
            min="0"
            value={this.props.info.capacity}
            onChange={this.updateCapacity} />
                </div>
                </div>;
        } else {
            header =
                <div className="column card-header">
                    {this.title} {this.props.info.id}
                </div>;
        }

        return (
            <div className="card text-center m-2">
              
              {header}

              <ul className="list-group list-group-flush">
                {rol}
              </ul>

              <div className="card-footer">
                <input
                  onKeyPress={this.addROLRank}
                  type="text"
                  value={this.state.currentROLRankId}
                  onChange={this.updateROLRankId}>
                </input>
              </div>
            </div>
        );
    }
}

class Applicant extends Participant {
    constructor(props) {
        super(props, "Applicant", 0);
    }
}

Applicant = connect(
    undefined,
    { addROL: addApplicantROL }
)(Applicant);

class Program extends Participant {
    constructor(props, capacity) {
        super(props, "Program", capacity);
    }
}

Program = connect(
    undefined,
    { addROL: addProgramROL, updateCapacity: updateProgramCapacity }
)(Program);

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let applicants = Object.values(this.props.applicants).map((x) => {
            return <Applicant key={x["id"]} info={x} />;
        });


        let programs = Object.values(this.props.programs).map((x) => {
            return <Program key={x["id"]} info={x} />;
        });


        let output;
        
        if (Object.keys(this.props.applicants).length > 0 &&
            Object.keys(this.props.programs).length > 0) {
            output = this.props.output_function(JSON.stringify(this.props.applicants),
                                                JSON.stringify(this.props.programs));

            output = JSON.parse(output);

            if (Array.isArray(output)) {
                output = output.join("\n");
            } else {
                alert("An error occurred: " + output.error);
                output = undefined;
            }
        }


        return (
            <div className="container">
              <div className="row">
                <div className="header">
                  <h1>Applicants</h1>
                  <button
                    className="btn btn-primary"
                    onClick={(e) => {
                          this.props.addApplicant();
                    }}>
                    Add
                  </button>
                </div>
              </div>

              <div className="row">
                {applicants}
              </div>

              <div className="row">
                <div className="header">
                  <h1>Programs</h1>
                  <button
                    className="btn btn-primary"
                    onClick={(e) => {
                        this.props.addProgram(2);
                    }}>
                    Add
                  </button>
                </div>
              </div>

              <div className="row">
                {programs}
              </div>
              
              <textarea value={output} readOnly rows="20" className="w-100"></textarea>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        applicants: state.applicants,
        programs: state.programs,
    };
};

export default connect(
    mapStateToProps,
    { addProgram, addApplicant }
)(App);
