import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { createStore } from "redux";

import App from "./App";
import rootReducer from "./store";

const store = createStore(rootReducer);

import {run_match_with_input, wasmBooted} from '../src/lib.rs';

wasmBooted.then(carms => {
    ReactDOM.render(
        <Provider store={store}>
          <App output_function={run_match_with_input}/>
        </Provider>,
        document.getElementById("root")
    );
});
