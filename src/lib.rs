mod utils;

use std::vec::Vec;
use std::collections::HashMap;
use wasm_bindgen::prelude::*;
use roth_peranson::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
	// Use `js_namespace` here to bind `console.log(..)` instead of just
	// `log(..)`
	#[wasm_bindgen(js_namespace = console)]
	fn log(s: &str);

	// The `console.log` is quite polymorphic, so we can bind it with multiple
	// signatures. Note that we need to use `js_name` to ensure we always call
	// `log` in JS.
	#[wasm_bindgen(js_namespace = console, js_name = log)]
	fn log_u32(a: u32);

	// Multiple arguments too!
	#[wasm_bindgen(js_namespace = console, js_name = log)]
	fn log_many(a: &str, b: &str);
}

#[wasm_bindgen]
pub fn run_match_default() -> String {
    let applicants: HashMap<i32, Applicant> = vec![
        ("Anderson", make_rol_program(&[1])),
        ("Beaudry", make_rol_program(&[1, 0])),
        ("Chen", make_rol_program(&[1, 0])),
        ("Davis", make_rol_program(&[0, 1, 2, 3])),
        ("Eastman", make_rol_program(&[1, 0, 3, 2])),
        ("Feldman", make_rol_program(&[1, 2, 0, 3])),
        ("Garcia", make_rol_program(&[1, 0, 3, 2])),
        ("Hassan", make_rol_program(&[3, 1, 0, 2]))
    ].iter()
    .enumerate()
    .map(|(i, (name, rol))|
         (i as i32, Applicant {
             id: i as i32,
             name: name.to_string(),
             rol: rol.to_vec(),
             pending: true,
             matched: None,
         }))
    .collect();

    let programs: HashMap<i32, Program> = vec![
        ("Mercy", make_rol_applicant(&[2, 6])),
        ("City", make_rol_applicant(&[6, 7, 4, 0, 1, 2, 3, 5])),
        ("General", make_rol_applicant(&[1, 4, 7, 0, 2, 3, 6])),
        ("State", make_rol_applicant(&[1, 4, 0, 2, 7, 5, 3, 6])),
    ].iter()
        .enumerate()
        .map(|(i, (name, rol))|
             (i as i32, Program {
                 id: i as i32,
                 name: name.to_string(),
                 rol: rol.to_vec(),
                 capacity: 2,
                 matches: Vec::with_capacity(2),
             }))
    .collect();

    return run_match(applicants, programs);
}

#[wasm_bindgen]
pub fn run_match_with_input(a: String, p: String) -> String {
    let applicants: HashMap<i32, Applicant> = match serde_json::from_str(&a) {
        Ok(x) => x,
        Err(e) => return format!("{{\"error\": \"{}\"}}", e),
    };

    // log(&format!("applicants: {:?}", applicants));

    let programs: HashMap<i32, Program> = match serde_json::from_str(&p) {
        Ok(x) => x,
        Err(e) => return format!("{{\"error\": \"{}\"}}", e),
    };

    // log(&format!("programs: {:?}", programs));

    return run_match(applicants, programs);
}

pub fn run_match(
    mut applicants: HashMap<i32, Applicant>,
    mut programs: HashMap<i32, Program>)
    -> String {
    let mut output: Vec<String> = Vec::new();
    output.push("---- RUNNING MATCH ALGORITHM ----".to_string());
    run_match_algo(&mut programs, &mut applicants, &mut output);

    output.push("---- TESTING MATCH STABILITY ----".to_string());
    if ! ensure_match_stability(&programs, &applicants, &mut output) {
        output.push(format!("MATCH IS UNSTABLE!"));
    }

    output.push("--------- FINAL RESULT ---------".to_string());
    output.push("--- APPLICANT MATCHES ---".to_string());
    for applicant in applicants.values() {
        if let Some(r) = &applicant.matched {
            output.push(format!("applicant {} matched to program {}", applicant.id, r.id));
        } else {
            output.push(format!("applicant {} is unmatched", applicant.id));
        }
    }

    output.push("--- PROGRAM MATCHES ---".to_string());
    for program in programs.values() {
        for m in program.matches.iter() {
            output.push(format!("program {} matched to applicant {}", program.id, m.id));
        }
    }

    log(&format!("run_match completed"));

    match serde_json::to_string(&output) {
        Ok(v) => {
            return v;
        },
        
        Err(x) => {
            log(&format!("failed to serialize: {}", x));
            return "nothing".to_string();
        }
    }
}
